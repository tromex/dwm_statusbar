# dwm_statusbar
# See LICENSE file for copyright and license details.

include config.mk

SRC = dwm_statusbar.c
OBJ = $(SRC:.c=.o)

all: options dwm_statusbar

options:
	@echo dwm_statusbar build options:
	@echo "CFLAGS   = $(CFLAGS)"
	@echo "LDFLAGS  = $(LDFLAGS)"
	@echo "CC       = $(CC)"

.c.o:
	$(CC) -c $(CFLAGS) $<

$(OBJ): config.h config.mk

dwm_statusbar: dwm_statusbar.o
	$(CC) -o $@ dwm_statusbar.o $(LDFLAGS)

clean:
	rm -f dwm_statusbar

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f dwm_statusbar $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/dwm_statusbar
	#mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	#sed "s/VERSION/$(VERSION)/g" < dwm_statusbar.1 > $(DESTDIR)$(MANPREFIX)/man1/dwm_statusbar.1
	#chmod 644 $(DESTDIR)$(MANPREFIX)/man1/dwm_statusbar.1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/dwm_statusbar\
		$(DESTDIR)$(MANPREFIX)/man1/dwm_statusbar.1

.PHONY: all options clean install uninstall

