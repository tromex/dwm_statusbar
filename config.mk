# dwm_statusbar version
VERSION = 0.1

# paths
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

# includes and libs
#INCS =
LIBS = -lm

# flags
CPPFLAGS = -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_XOPEN_SOURCE=700 -D_POSIX_C_SOURCE=200809L -DVERSION=\"$(VERSION)\"
#CFLAGS   = -std=c99 -pedantic -Wall -Os $(CPPFLAGS)
CFLAGS   = -std=c99 -pedantic -Wall -ggdb $(CPPFLAGS)
LDFLAGS  = $(LIBS)

# compiler and linker
CC = cc
