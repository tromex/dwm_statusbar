
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#include "config.h"

enum Result {
    SUCCESS = 0,
    ERROR,
};

volatile int running = 1;

#define STATUSBAR_LEN 255
static char statusbar[STATUSBAR_LEN];
static char *cursor;

// Temporary buffer meant to be reused by many function (use carefully)
static char tmp_buf[50];

int write_separator();
int write_battery();
int write_time();

int read_file_with_int(const char *file, int* result);
void xsetroot_name(const char *name);
void die(const char *errstr, ...);


int
read_file_with_int(const char *file, int* result)
{
    int fd, read_cnt;
    // read from file
    fd = open(file, O_RDONLY);
    if (fd == -1)
        return ERROR;
    read_cnt = read(fd, tmp_buf, sizeof(tmp_buf) - 1);
    close(fd);
    // convert to int
    tmp_buf[read_cnt] = '\0';
    *result = atoi(tmp_buf);
    return SUCCESS;
}

void
xsetroot_name(const char *name)
{
    switch(fork()) {
    case -1:
        die("fork failed: %s\n", strerror(errno));
    case 0:
        execlp("xsetroot", "xsetroot", "-name", name, NULL);
        die("execlp failed\n");
    }
}

void
die(const char *errstr, ...)
{
    va_list ap;

    va_start(ap, errstr);
    vfprintf(stderr, errstr, ap);
    va_end(ap);
    exit(1);
}

int
write_separator()
{
    int written;
    size_t size = STATUSBAR_LEN - (cursor - statusbar);
    written = snprintf(cursor, size, " | ");
    cursor += written;
    return written;
}

int
write_battery()
{
    int written, value;
    size_t size;

    if (read_file_with_int(battery_capacity, &value) == ERROR)
        return 0;

    size = STATUSBAR_LEN - (cursor - statusbar);
    written = snprintf(cursor, size, "BAT0: %d%%", value);
    cursor += written;
    return written;
}

int
write_time()
{
    //TODO check for buffer overrun: statusbar[STATUSBAR_LEN - 1] - cursor
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    int written;
    size_t size;

    size = STATUSBAR_LEN - (cursor - statusbar);
    written = snprintf(cursor, size, "%02d:%02d:%02d", tm.tm_hour, tm.tm_min, tm.tm_sec);
    cursor += written;
    return written;
}

void
write_termination()
{
    //TODO check for buffer overrun: statusbar[STATUSBAR_LEN - 1] - cursor
    *cursor++ = '\0';
}

int main()
{
    do {
        cursor = &statusbar[0];

        if (write_battery() != 0)
            write_separator();
        write_time();
        write_termination();

        xsetroot_name(statusbar);
        sleep(1); // TODO use nanosleep and make the refresh period configurable
    } while (running);
}

